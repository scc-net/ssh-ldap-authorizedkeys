#!/usr/bin/env python3
import argparse
import configparser
import os
import sys

import ldap3

defaults = {
    'config': '/etc/ssh/ldap-authorizedkeys.config'
}

def main():
    parser = argparse.ArgumentParser(description='AuthorizedKeysCommand for sshd')
    parser.add_argument('-C', '--config', metavar='configfile', default=defaults['config'], type=str, help='Path to config file')
    parser.add_argument('account', metavar='account', type=str, help='Account')
    args = parser.parse_args()

    if os.stat(args.config).st_mode & 0o0077:
        print(f'Only owner should be allowed to read file {args.config}')
        sys.exit(1)

    config = configparser.ConfigParser()
    with open(args.config, 'r') as f:
        config.read_file(f)

    ldap3.set_config_parameter('POOLING_LOOP_TIMEOUT', 5)
    server_pool = ldap3.ServerPool(None, ldap3.ROUND_ROBIN, active=3, exhaust=True)
    for uri in config.get('ldap','uri').split(' '):
        server = ldap3.Server(uri)
        server_pool.add(server)

    conn = ldap3.Connection(server_pool, config.get('ldap', 'user'), config.get('ldap', 'password'), receive_timeout=3)
    conn.bind()

    account_name = ldap3.utils.dn.escape_rdn(args.account)
    search_base = config.get('filter', 'users')
    search_filter = "(&(objectClass=%s)(%s=%s))" % (
        config.get('filter', 'objectclass'),
        config.get('filter', 'userattr'),
        account_name)


    pubkey_attribute_name = config.get('filter', 'pubkeyattr')
    res = conn.search(search_base, search_filter, search_scope=ldap3.SUBTREE, attributes=pubkey_attribute_name)

    if not res:
        conn.unbind()
        sys.exit(1)

    if len(conn.entries) == 0:
        conn.unbind()
        sys.exit(0)

    if len(conn.entries) > 1:
        print(f'LDAP search did return multiple users for account {args.account}')
        conn.unbind()
        sys.exit(1)

    entry = conn.entries[0]

    if entry[pubkey_attribute_name] is None:
        conn.unbind()
        sys.exit(0)

    for ssh_key in entry[pubkey_attribute_name]:
        print(ssh_key)

    conn.unbind()


if __name__ == '__main__':
    main()
