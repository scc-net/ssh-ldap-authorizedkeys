# Configuration
See example.config. Configuration must be saved in
/etc/ssh/ldap-ldap-authorizedkeys.config because AuthorizedKeysCommand doesn't
accept options.
# Usage
Configure 'AuthorizedKeysCommand /path/to/ldap-authorizedkeys.py' in sshd_config.
Maybe you have to change AuthorizedKeysCommandUser, too.
